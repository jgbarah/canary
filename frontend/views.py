import datetime
import json
import random
import string

from django.shortcuts import render

sentences = [
    ["", "Oh, great!", "Good.", "Awesome!!!", "Perfect!"],
    ["You're now",
     "You're almost there. You can now follow on,",
     "So you still want to check your luck? You're clearly",
     "You know you're",
     "Follow the path,"
     ],
    ["headed for", "nearing", "on the road to", "proceeding towards", "pointing to", "close to"],
    ["final domination.", "getting something really interesting.",
     "your well deserved reward.", "something really, really fantastic.",
     "getting what you deserve.", "making your dreams come true."]
]

logfile_name = "log.json"

def resource(request, name):

    next = ''.join(random.choices(string.ascii_lowercase, k=16))
    headers = dict((header[5:], value) for (header, value) in request.META.items()
        if header.startswith('HTTP_'))
    output = {
        'resource': request.path,
        'next': next,
        'scheme': request.scheme,
        'date': str(datetime.datetime.now()),
        'method': request.method,
        'client_ip': request.META.get('HTTP_X_FORWARDED_FOR',
                                      request.META.get('REMOTE_ADDR', ''))
            .split(',')[-1].strip(),
        'headers': headers,
        'cookies': request.COOKIES,
        'query_post': dict(request.POST),
        'query_get': dict(request.GET)
    }

    with open(logfile_name, "a") as file:
        file.write(json.dumps(output, separators=(',', ':')) + '\n')

    return render(request, 'frontend/resource.html', {'sentences': sentences,
                                                      'resource': next})