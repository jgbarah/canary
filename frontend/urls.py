"""frontend app URL Configuration

"""

from django.urls import path
from . import views

urlpatterns = [
    path('', views.resource, {'name': ''}, name='resource'),
    path('<path:name>', views.resource, name='resource'),
]